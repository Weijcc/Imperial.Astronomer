#pragma once

#include <QJsonDocument>
#include <QListWidgetItem>

#include "Imperial.Astronomer.Obtain.Resources.h"
#include "ui_Imperial.Astronomer.Record.List.Widget.h"

class ImperialAstronomerRecordListWidget : public QWidget {
    Q_OBJECT
private:
    Ui::ImperialAstronomerRecordListWidgetUI *userInterface = nullptr;
    ImperialAstronomerObtainResources obtainResources;
    QVector<QListWidgetItem*> proficiencyListWidgetItems = QVector<QListWidgetItem*>(10);

public:
    ImperialAstronomerRecordListWidget(QWidget *parent = nullptr);
    ~ImperialAstronomerRecordListWidget(void);

public slots:
    void updateSummonerInformation(QJsonDocument responseJsonDocument);
};
