#pragma once

#include <QTimer>
#include <QThread>
#include <QMouseEvent>

#include "ui_Imperial.Astronomer.Main.Window.h"
#include "Imperial.Astronomer.Record.List.Widget.h"
#include "Imperial.Astronomer.League.Client.Ux.API.h"

class ImperialAstronomerMainWindow : public QMainWindow {
    Q_OBJECT
private:
    bool leftGroupBoxIsEnable = false;
    QTimer *leftGroupBoxMoveTimer = nullptr;
    bool movePositionFlag = false;
    QPointF movePosition;
    QThread *leagueClientUxAPIBaseThread = nullptr;
    Ui::ImperialAstronomerMainWindowUI *userInterface = nullptr;
    ImperialAstronomerRecordListWidget *recordListWidget = nullptr;
    ImperialAstronomerLeagueClientUxAPI *leagueClientUxAPI = nullptr;

protected:
    bool event(QEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

public:
    ImperialAstronomerMainWindow(QMainWindow *parent = nullptr);
    ~ImperialAstronomerMainWindow(void);
};
