#pragma once

#include <QObject>
#include <QJsonDocument>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkAccessManager>

class ImperialAstronomerLeagueClientUxAPI : public QObject {
    Q_OBJECT
private:
    qint32 port;
    QString directory;
    QString authorization;
    QNetworkRequest networkRequest;
    QNetworkAccessManager *networkAccessManager = nullptr;

    QJsonDocument request(const QNetworkAccessManager::Operation &operation, const QUrl &url, const QString &data = QString());

public:
    ImperialAstronomerLeagueClientUxAPI(QObject *parent = nullptr);
    ~ImperialAstronomerLeagueClientUxAPI(void);

    QJsonDocument obtainSummonerInformation(void);

public slots:
    void execute(void);

signals:
    void readyObtainSummonerInformation(QJsonDocument responseJsonDocument);
};
