#pragma once

#include <QPixmap>
#include <QObject>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkAccessManager>

class ImperialAstronomerObtainResources : public QObject {
    Q_OBJECT
private:
    QNetworkRequest networkRequest;
    QNetworkAccessManager *networkAccessManager = nullptr;
    QByteArray request(const QNetworkAccessManager::Operation& operation, const QUrl& url, const QString& data = QString());

public:
    ImperialAstronomerObtainResources(QObject *parent = nullptr);
    ~ImperialAstronomerObtainResources(void);

    QPixmap obtainProfileIcon(const qint32 &id);
    QPixmap obtainChampionIcon(const qint32 &id);
    QPixmap updatePixmapRound(QPixmap pixmap, const QSize &size, const quint32 &radius);
};