#include <QDebug>
#include <QThread>
#include <QProcess>
#include <QEventLoop>
#include <QApplication>
#include <QJsonDocument>
#include <QRegularExpression>

#include "Imperial.Astronomer.League.Client.Ux.API.h"

ImperialAstronomerLeagueClientUxAPI::ImperialAstronomerLeagueClientUxAPI(QObject *parent) : QObject(parent) {
    QString commandLine;
    QString arguments = "Get-CIMInstance Win32_Process -Filter \"NAME = 'LeagueClientUx.exe'\" | Select CommandLine | ConvertTo-Csv";
    QProcess process;
    QRegularExpression regularExpression;
    process.start("C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe", arguments.split(" ", Qt::SkipEmptyParts));
    process.waitForFinished();
    commandLine = process.readAllStandardOutput();
    if (!commandLine.isEmpty()) {
        regularExpression.setPattern("--app-port=([0-9]*)");
        this->port = regularExpression.match(commandLine).captured(1).toInt();
        regularExpression.setPattern("--install-directory=([a-z]:(\\[\\w\u4E00-\u9FA5..\\s]*)*[.\\w\\\\u4E00-\u9FA5\\s]*)");
        this->directory = regularExpression.match(commandLine).captured(1);
        regularExpression.setPattern("--remoting-auth-token=([\\w-]*)");
        this->authorization = regularExpression.match(commandLine).captured(1);
    }
    //qDebug() << this->port;
    //qDebug() << this->directory;
    //qDebug() << this->authorization;
    //auto sslConfiguration = this->networkRequest.sslConfiguration();
    //sslConfiguration.setProtocol(QSsl::TlsV1_3);
    //sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyNone);
    //this->networkAccessManager = new QNetworkAccessManager(this);
    //this->networkAccessManager->connectToHostEncrypted("https://127.0.0.1", this->port, sslConfiguration);
    //this->networkRequest.setRawHeader("Accept", "application/json");
    //this->networkRequest.setRawHeader("Content-Type", "application/json");
    //this->networkRequest.setRawHeader("Connection", "keep-alive");
    //this->networkRequest.setRawHeader("Authorization", QByteArray("Basic " + QByteArray("riot:" + this->authorization.toUtf8()).toBase64()));
    //this->networkRequest.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);
    //this->networkRequest.setSslConfiguration(sslConfiguration);
}

ImperialAstronomerLeagueClientUxAPI::~ImperialAstronomerLeagueClientUxAPI(void) {
}

QJsonDocument ImperialAstronomerLeagueClientUxAPI::request(const QNetworkAccessManager::Operation &operation,
                                                           const QUrl &url, const QString &data) {
    QEventLoop eventLoop;
    QNetworkReply* networkReply = nullptr;
    this->networkRequest.setUrl(url);
    switch (operation)
    {
        case QNetworkAccessManager::HeadOperation:
            break;
        case QNetworkAccessManager::GetOperation:
            networkReply = this->networkAccessManager->get(this->networkRequest);
            break;
        case QNetworkAccessManager::PutOperation:
            break;
        case QNetworkAccessManager::PostOperation:
            networkReply = this->networkAccessManager->post(this->networkRequest, data.toUtf8());
            break;
        case QNetworkAccessManager::DeleteOperation:
            break;
        case QNetworkAccessManager::CustomOperation:
            networkReply = this->networkAccessManager->sendCustomRequest(this->networkRequest, "PATCH", data.toUtf8());
            break;
        case QNetworkAccessManager::UnknownOperation:
            break;
        default:
            return QJsonDocument();
    }
    if (networkReply) {
        connect(networkReply, &QNetworkReply::finished, &eventLoop, &QEventLoop::quit);
        eventLoop.exec();
        if (networkReply->error() == QNetworkReply::NoError) {
            return QJsonDocument::fromJson(networkReply->readAll());
        }
    }
    return QJsonDocument();
}

QJsonDocument ImperialAstronomerLeagueClientUxAPI::obtainSummonerInformation(void) {
    for (auto retry = 0; retry < 5; retry++) {
        auto responseJsonDocument = this->request(QNetworkAccessManager::GetOperation,
                                                  QUrl("https://127.0.0.1:" + QString::number(this->port) + "/lol-summoner/v1/current-summoner"));
        if (!responseJsonDocument.isEmpty()) {
            qDebug().noquote() << responseJsonDocument.toJson();
            emit readyObtainSummonerInformation(responseJsonDocument);
            return responseJsonDocument;
        }
    }
    return QJsonDocument();
}

void ImperialAstronomerLeagueClientUxAPI::execute(void) {
    while (true) {
        QThread::msleep(1000);
    }
}
