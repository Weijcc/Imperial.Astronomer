#include <QFile>
#include <QPainter>
#include <QEventLoop>
#include <QPainterPath>

#include "Imperial.Astronomer.Obtain.Resources.h"

ImperialAstronomerObtainResources::ImperialAstronomerObtainResources(QObject *parent) : QObject(parent) {
    auto sslConfiguration = this->networkRequest.sslConfiguration();
    sslConfiguration.setProtocol(QSsl::TlsV1_3);
    sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyNone);
    this->networkRequest.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);
    this->networkRequest.setSslConfiguration(sslConfiguration);
    this->networkAccessManager = new QNetworkAccessManager(this);
}

ImperialAstronomerObtainResources::~ImperialAstronomerObtainResources(void) {

}

QByteArray ImperialAstronomerObtainResources::request(const QNetworkAccessManager::Operation &operation, const QUrl &url,
                                                   const QString &data) {
    QEventLoop eventLoop;
    QNetworkReply* networkReply = nullptr;
    this->networkRequest.setUrl(url);
    switch (operation) {
        case QNetworkAccessManager::HeadOperation:
            break;
        case QNetworkAccessManager::GetOperation:
            networkReply = this->networkAccessManager->get(this->networkRequest);
            break;
        case QNetworkAccessManager::PutOperation:
            break;
        case QNetworkAccessManager::PostOperation:
            networkReply = this->networkAccessManager->post(this->networkRequest, data.toUtf8());
            break;
        case QNetworkAccessManager::DeleteOperation:
            break;
        case QNetworkAccessManager::CustomOperation:
            networkReply = this->networkAccessManager->sendCustomRequest(this->networkRequest, "PATCH", data.toUtf8());
            break;
        case QNetworkAccessManager::UnknownOperation:
            break;
        default:
            return QByteArray();
    }
    if (networkReply) {
        connect(networkReply, &QNetworkReply::finished, &eventLoop, &QEventLoop::quit);
        eventLoop.exec();
        if (networkReply->error() == QNetworkReply::NoError) {
            return networkReply->readAll();
        }
    }
    return QByteArray();
}

QPixmap ImperialAstronomerObtainResources::updatePixmapRound(QPixmap pixmap, const QSize &size, const quint32 &radius) {
    qint32 pixmapWidth = size.width();
    qint32 pixmapHeight = size.height();
    QPixmap newPixmap = pixmap.scaled(pixmapWidth, (pixmapHeight == 0 ? pixmapWidth : pixmapHeight), 
        Qt::KeepAspectRatio, Qt::SmoothTransformation);
    QPixmap pixmapResult(pixmapWidth, pixmapHeight);
    pixmapResult.fill(Qt::transparent);
    QPainter painter(&pixmapResult);
    painter.setRenderHints(QPainter::Antialiasing, true);
    painter.setRenderHints(QPainter::SmoothPixmapTransform, true);
    QPainterPath painterPath;
    QRect Rect(0, 0, pixmapWidth, pixmapHeight);
    painterPath.addRoundedRect(Rect, radius, radius);
    painter.setClipPath(painterPath);
    painter.drawPixmap(0, 0, pixmapWidth, pixmapHeight, newPixmap);
    return pixmapResult;
}

QPixmap ImperialAstronomerObtainResources::obtainProfileIcon(const qint32 &id) {
    auto profileIconId = (id < 80 ? (id + 50) : id);
    auto pixmapData = this->request(QNetworkAccessManager::GetOperation, QUrl(
    "https://raw.communitydragon.org/latest/game/assets/ux/summonericons/profileicon" + QString::number(profileIconId) + ".png"));
    QPixmap pixmap;
    pixmap.loadFromData(pixmapData, "png");
    return pixmap;
}

QPixmap ImperialAstronomerObtainResources::obtainChampionIcon(const qint32 &id) {
    auto PixmapData = this->request(QNetworkAccessManager::GetOperation, QUrl(
    "https://raw.communitydragon.org/latest/plugins/rcp-be-lol-game-data/global/default/v1/champion-icons/" + QString::number(id) + ".png"));
    QPixmap pixmap;
    pixmap.loadFromData(PixmapData, "png");
    return pixmap;
}
