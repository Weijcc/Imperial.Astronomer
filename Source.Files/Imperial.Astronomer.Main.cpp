#include <QFile>
#include <QApplication>
#include <QFontDatabase>

#include "Imperial.Astronomer.Main.Window.h"

int main(int argc, char *argv[]) {
    QApplication application(argc, argv);
    QFile styleFile(":/Styles/Imperial.Astronomer.Style.css");
    QFontDatabase::addApplicationFont(":/Fonts/HarmonyOS_Sans_SC_Medium.ttf");
    if (styleFile.open(QIODevice::ReadOnly)) {
        application.setStyleSheet(styleFile.readAll());
        styleFile.close();
    }
    ImperialAstronomerMainWindow window;
    window.show();
    return application.exec();
}
