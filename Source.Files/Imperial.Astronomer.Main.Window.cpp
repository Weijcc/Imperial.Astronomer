#include <QGraphicsDropShadowEffect>

#include "Imperial.Astronomer.Main.Window.h"

ImperialAstronomerMainWindow::ImperialAstronomerMainWindow(QMainWindow *parent) :
        QMainWindow(parent), userInterface(new Ui::ImperialAstronomerMainWindowUI), leftGroupBoxMoveTimer(new QTimer(this)) {
    this->userInterface->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setAttribute(Qt::WA_Hover);
    auto *dropShadowEffect = new QGraphicsDropShadowEffect(this);
    dropShadowEffect->setBlurRadius(5);
    dropShadowEffect->setOffset(0, 0);
    dropShadowEffect->setColor(QColor(33, 33, 33));
    this->userInterface->ImperialAstronomerMainFrameUI->setGraphicsEffect(dropShadowEffect);
    dropShadowEffect = new QGraphicsDropShadowEffect(this);
    dropShadowEffect->setBlurRadius(3);
    dropShadowEffect->setOffset(0, 0);
    dropShadowEffect->setColor(QColor(0, 0, 0));
    this->userInterface->ImperialAstronomerLeftGroupBoxUI->setGraphicsEffect(dropShadowEffect);
    dropShadowEffect = new QGraphicsDropShadowEffect(this);
    dropShadowEffect->setBlurRadius(3);
    dropShadowEffect->setOffset(0, 0);
    dropShadowEffect->setColor(QColor(0, 0, 0));
    this->userInterface->ImperialAstronomerExitButtonUI->setGraphicsEffect(dropShadowEffect);
    dropShadowEffect = new QGraphicsDropShadowEffect(this);
    dropShadowEffect->setBlurRadius(3);
    dropShadowEffect->setOffset(0, 0);
    dropShadowEffect->setColor(QColor(0, 0, 0));
    this->userInterface->ImperialAstronomerRecordListCheckBoxUI->setGraphicsEffect(dropShadowEffect);
    dropShadowEffect = new QGraphicsDropShadowEffect(this);
    dropShadowEffect->setBlurRadius(3);
    dropShadowEffect->setOffset(0, 0);
    dropShadowEffect->setColor(QColor(0, 0, 0));
    this->userInterface->ImperialAstronomerFunctionListCheckBoxUI->setGraphicsEffect(dropShadowEffect);
    dropShadowEffect = new QGraphicsDropShadowEffect(this);
    dropShadowEffect->setBlurRadius(3);
    dropShadowEffect->setOffset(0, 0);
    dropShadowEffect->setColor(QColor(0, 0, 0));
    this->userInterface->ImperialAstronomerInformationListCheckBoxUI->setGraphicsEffect(dropShadowEffect);
    dropShadowEffect = new QGraphicsDropShadowEffect(this);
    dropShadowEffect->setBlurRadius(5);
    dropShadowEffect->setOffset(0, 0);
    dropShadowEffect->setColor(QColor(0, 0, 0));
    this->userInterface->ImperialAstronomerLogoLabelUI->setGraphicsEffect(dropShadowEffect);
    this->userInterface->ImperialAstronomerLogoLabelUI->setPixmap(QPixmap(":/Images/Imperial.Astronomer.Information.Logo.png"));
    this->recordListWidget = new ImperialAstronomerRecordListWidget(this->userInterface->ImperialAstronomerMainFrameUI);
    this->recordListWidget->lower();
    this->userInterface->ImperialAstronomerLogoLabelUI->lower();
    this->leagueClientUxAPIBaseThread = new QThread();
    this->leagueClientUxAPI = new ImperialAstronomerLeagueClientUxAPI();
    this->leagueClientUxAPI->moveToThread(this->leagueClientUxAPIBaseThread);
    connect(this->leagueClientUxAPI, &ImperialAstronomerLeagueClientUxAPI::readyObtainSummonerInformation,
            this->recordListWidget, &ImperialAstronomerRecordListWidget::updateSummonerInformation);
    connect(this->leagueClientUxAPIBaseThread, &QThread::finished, this->leagueClientUxAPIBaseThread, &QObject::deleteLater);
    connect(this->leagueClientUxAPIBaseThread, &QThread::finished, this->leagueClientUxAPI, &QObject::deleteLater);
    connect(this->leagueClientUxAPIBaseThread, &QThread::started, this->leagueClientUxAPI, &ImperialAstronomerLeagueClientUxAPI::execute);
    connect(this->userInterface->ImperialAstronomerExitButtonUI, &QPushButton::clicked, this, [&](void){
        qApp->exit();
    });
    connect(this->leftGroupBoxMoveTimer, &QTimer::timeout, this, [&] (void) {
        auto LeftGroupBoxX = this->userInterface->ImperialAstronomerLeftGroupBoxUI->pos().x();
        if (this->leftGroupBoxIsEnable) {
            LeftGroupBoxX -= 5;
            this->userInterface->ImperialAstronomerLeftGroupBoxUI->move(LeftGroupBoxX,
                                            this->userInterface->ImperialAstronomerLeftGroupBoxUI->pos().y());
            if (LeftGroupBoxX == -80) {
                this->leftGroupBoxMoveTimer->stop();
                this->setAttribute(Qt::WA_Hover, true);
            }
        } else {
            LeftGroupBoxX += 5;
            this->userInterface->ImperialAstronomerLeftGroupBoxUI->move(LeftGroupBoxX,
                                            this->userInterface->ImperialAstronomerLeftGroupBoxUI->pos().y());
            if (LeftGroupBoxX == -5) {
                this->leftGroupBoxMoveTimer->stop();
                this->setAttribute(Qt::WA_Hover, true);
            }
        }
    });
    connect(this->userInterface->ImperialAstronomerRecordListCheckBoxUI, &QCheckBox::stateChanged, this, [&] (const int state) {
        if (state == Qt::Checked) {
            this->userInterface->ImperialAstronomerFunctionListCheckBoxUI->blockSignals(true);
            this->userInterface->ImperialAstronomerFunctionListCheckBoxUI->setCheckState(Qt::Unchecked);
            this->userInterface->ImperialAstronomerFunctionListCheckBoxUI->blockSignals(false);
            this->userInterface->ImperialAstronomerInformationListCheckBoxUI->blockSignals(true);
            this->userInterface->ImperialAstronomerInformationListCheckBoxUI->setCheckState(Qt::Unchecked);
            this->userInterface->ImperialAstronomerInformationListCheckBoxUI->blockSignals(false);
            this->recordListWidget->show();
            return;
        }
        this->recordListWidget->hide();
    });
    connect(this->userInterface->ImperialAstronomerFunctionListCheckBoxUI, &QCheckBox::stateChanged, this, [&] (const int state) {
        if (state == Qt::Checked) {
            this->userInterface->ImperialAstronomerRecordListCheckBoxUI->blockSignals(true);
            this->userInterface->ImperialAstronomerRecordListCheckBoxUI->setCheckState(Qt::Unchecked);
            this->userInterface->ImperialAstronomerRecordListCheckBoxUI->blockSignals(false);
            this->userInterface->ImperialAstronomerInformationListCheckBoxUI->blockSignals(true);
            this->userInterface->ImperialAstronomerInformationListCheckBoxUI->setCheckState(Qt::Unchecked);
            this->userInterface->ImperialAstronomerInformationListCheckBoxUI->blockSignals(false);
            this->recordListWidget->hide();
        }
    });
    connect(this->userInterface->ImperialAstronomerInformationListCheckBoxUI, &QCheckBox::stateChanged, this, [&] (const int state) {
        if (state == Qt::Checked) {
            this->userInterface->ImperialAstronomerRecordListCheckBoxUI->blockSignals(true);
            this->userInterface->ImperialAstronomerRecordListCheckBoxUI->setCheckState(Qt::Unchecked);
            this->userInterface->ImperialAstronomerRecordListCheckBoxUI->blockSignals(false);
            this->userInterface->ImperialAstronomerFunctionListCheckBoxUI->blockSignals(true);
            this->userInterface->ImperialAstronomerFunctionListCheckBoxUI->setCheckState(Qt::Unchecked);
            this->userInterface->ImperialAstronomerFunctionListCheckBoxUI->blockSignals(false);
            this->recordListWidget->hide();
        }
    });
    this->leftGroupBoxIsEnable = (this->userInterface->ImperialAstronomerLeftGroupBoxUI->pos().x() > -80);
    this->leftGroupBoxMoveTimer->start(20);
    this->leagueClientUxAPIBaseThread->start();
}

ImperialAstronomerMainWindow::~ImperialAstronomerMainWindow(void) {
    delete this->userInterface;
}

bool ImperialAstronomerMainWindow::event(QEvent *event) {
    if (event->type() == QEvent::HoverEnter || event->type() == QEvent::HoverLeave || event->type() == QEvent::HoverMove) {
        auto *hoverEvent = reinterpret_cast<QHoverEvent*>(event);
        auto position = hoverEvent->globalPosition() - this->pos();
        this->leftGroupBoxIsEnable = (this->userInterface->ImperialAstronomerLeftGroupBoxUI->pos().x() > -80);
        if ((position.x() < 60) && (position.y() > 180) && (position.y() < 550) && !leftGroupBoxIsEnable) {
            this->setAttribute(Qt::WA_Hover, false);
            this->leftGroupBoxMoveTimer->start(1);
        } else if (((position.x() > 100) || (position.y() < 180) || (position.y() > 550)) && leftGroupBoxIsEnable) {
            this->setAttribute(Qt::WA_Hover, false);
            this->leftGroupBoxMoveTimer->start(1);
        }
    }
    return QMainWindow::event(event);
}

void ImperialAstronomerMainWindow::mousePressEvent(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton) {
        this->movePositionFlag = true;
        this->movePosition = event->globalPosition() - this->pos();
        event->accept();
    }
}

void ImperialAstronomerMainWindow::mouseMoveEvent(QMouseEvent *event) {
    if (this->movePositionFlag && (event->buttons() & Qt::LeftButton)) {
        this->move((event->globalPosition() - this->movePosition).toPoint());
        event->accept();
    }
}

void ImperialAstronomerMainWindow::mouseReleaseEvent(QMouseEvent *) {
    this->movePositionFlag = false;
}
