#include <QJsonValue>
#include <QJsonObject>
#include <QPushButton>
#include <QGraphicsDropShadowEffect>

#include "Imperial.Astronomer.Record.List.Widget.h"

ImperialAstronomerRecordListWidget::ImperialAstronomerRecordListWidget(QWidget *parent) :
        QWidget(parent), userInterface(new Ui::ImperialAstronomerRecordListWidgetUI) {
    this->userInterface->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->move(0, 0);
    this->hide();
    auto *dropShadowEffect = new QGraphicsDropShadowEffect(this);
    dropShadowEffect->setBlurRadius(5);
    dropShadowEffect->setOffset(0, 0);
    dropShadowEffect->setColor(QColor(33, 33, 33));
    this->userInterface->ImperialAstronomerSummonerProfileIconLabelUI->setGraphicsEffect(dropShadowEffect);
    dropShadowEffect = new QGraphicsDropShadowEffect(this);
    dropShadowEffect->setBlurRadius(5);
    dropShadowEffect->setOffset(0, 0);
    dropShadowEffect->setColor(QColor(33, 33, 33));
    this->userInterface->ImperialAstronomerSummonerNameLabelUI->setGraphicsEffect(dropShadowEffect);
    dropShadowEffect = new QGraphicsDropShadowEffect(this);
    dropShadowEffect->setBlurRadius(5);
    dropShadowEffect->setOffset(0, 0);
    dropShadowEffect->setColor(QColor(33, 33, 33));
    this->userInterface->ImperialAstronomerSummonerOtherInfoLabelUI->setGraphicsEffect(dropShadowEffect);
    auto profileIconPixmap = this->obtainResources.updatePixmapRound(
            this->obtainResources.obtainProfileIcon(783), QSize(70, 70), 35);
    this->userInterface->ImperialAstronomerSummonerProfileIconLabelUI->setPixmap(profileIconPixmap);
    for (auto* proficiencyListWidgetItem : this->proficiencyListWidgetItems) {
        proficiencyListWidgetItem = new QListWidgetItem(this->userInterface->ImperialAstronomerSummonerProficiencyListWidgetUI);
        proficiencyListWidgetItem->setForeground(QColor("#FAFAFA"));
        proficiencyListWidgetItem->setText("111111111111111111");
        proficiencyListWidgetItem->setIcon(QIcon(":/Images/Imperial.Astronomer.Exit.Button.Hover.png"));
    }
}

ImperialAstronomerRecordListWidget::~ImperialAstronomerRecordListWidget(void) {
    delete this->userInterface;
}

void ImperialAstronomerRecordListWidget::updateSummonerInformation(QJsonDocument responseJsonDocument) {
    QString summonerName = responseJsonDocument.object()["displayName"].toString() +
            "#" + responseJsonDocument.object()["tagLine"].toString();
    this->userInterface->ImperialAstronomerSummonerNameLabelUI->setText(summonerName);
    auto profileIcon = this->obtainResources.updatePixmapRound(
            this->obtainResources.obtainProfileIcon(responseJsonDocument.object()["profileIconId"].toInteger()), QSize(70, 70), 35);
    this->userInterface->ImperialAstronomerSummonerProfileIconLabelUI->setPixmap(profileIcon);
    this->userInterface->ImperialAstronomerSummonerOtherInfoLabelUI->setText(
            "等级：" + QString::number(responseJsonDocument.object()["summonerLevel"].toInteger()) + "（" +
            QString::number(responseJsonDocument.object()["xpSinceLastLevel"].toInteger()) + " / " +
            QString::number(responseJsonDocument.object()["xpUntilNextLevel"].toInteger()) + " - " +
            QString::number(responseJsonDocument.object()["percentCompleteForNextLevel"].toInteger()) + "%）\n" + "段位：N/A");
}
